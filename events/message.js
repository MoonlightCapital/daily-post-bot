module.exports = async (client, message) => {

  if (!message.content.startsWith(client.config.prefix)) return;
  if (message.author.bot) return;

  if(!message.member.hasPermission('ADMINISTRATOR'))
    return;

  const args = message.content.slice(client.config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();

  const cmd = client.commands.get(command);

  message.guild.promptsChannel = message.guild.channels.find(c => c.name == client.config.channels.prompts) || message.guild.channels.get(client.config.channels.prompts);

  if (!cmd) return;

  cmd.run(client, message, args);
};
