module.exports = async (client) => {

  console.log('I am ready!');
  console.log(`I am logged in as ${client.user.tag}`)
  console.log(`Node version: ${process.version}`)
  console.log(`Discord.JS version: ${client.version}`)

  //cronjob prompt execution

  const {RichEmbed} = require('discord.js');
  let embed = new RichEmbed();


  client.app.get('/message', (req, resp) => {
    let auth = req.query.key;
    //console.log(auth);

    if(auth !== client.config.secretkey) return resp.sendStatus(403);

    let list = client.db.get('daily.prompts');
    let element = list.filter(e=>e.used == false)[0];
    if(!element) return resp.sendStatus(404);

    let guild = client.guilds.first();
    let channel = guild.channels.find(c=>c.name == client.config.channels.posts) || guild.channels.get(client.config.channels.posts);
    if(!channel || !element) return;

    embed
    .setColor(0xFF0000)
    .setDescription(element.content)
    .addField('Word of the day', client.db.get('daily.word'))

    channel.send({embed:embed});

    let index = list.indexOf(element);
    element.used = true;
    list.splice(index, 1);
    client.db.set('daily.prompts', list);
    embed.fields = [];

    resp.sendStatus(201);
  });

};
