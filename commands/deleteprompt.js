exports.run = async (client, message, args) => {

  let id = parseInt(args[0]);

  if(!id)
    return message.channel.send(':no_entry_sign: Invalid ID');

  let list = client.db.get('daily.prompts');

  let element = list.filter(p=>p.id == id)[0];

  if(!element) return message.channel.send(':x: Prompt not found');

  const {RichEmbed} = require('discord.js');

  let embed = new RichEmbed()

  .setDescription(element.content)
  .setColor(0xFF0000)
  .setTitle('Candidate for deletion')

  .setFooter('ID: '+element.id)
  .setTimestamp(new Date());

  message.channel.send(`:bangbang: To confirm deletion of the following prompt, please confirm the operation with: \`confirm\` or \`cancel\``,
  {embed:embed})

  const filter = m => m.author.id == message.author.id && (m.content == 'confirm' || m.content == 'cancel');
  message.channel.awaitMessages(filter, { max: 1, time: 30000, errors: ['time'] })
    .then(collected => {
      let msg = collected.first()

      if(msg.content == 'confirm') {
        let index = list.indexOf(element);
        if (index > -1) {
          list.splice(index, 1);
          client.db.set('daily.prompts', list);
          message.channel.send(':wastebasket: Prompt deleted successfully!')
        }

      } else {
        message.channel.send(':diamond_shape_with_a_dot_inside: Operation cancelled.')
      }

    })
    .catch(collected => message.channel.send(':warning: No input given in 30 seconds. Operation aborted'));
};
