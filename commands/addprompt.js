exports.run = async (client, message, args) => {

  let text = args.join(' ');

  if(text.length > 1024)
    return message.reply(':x: The prompt is too long! It should be 1024 characters or less.');

  let index = client.db.get('daily.nextpromptindex');

  let prompt = {
    id: index,
    content: text,
    used: false
  };

  client.db.push('daily.prompts', prompt);
  client.db.add('daily.nextpromptindex', 1)

  await message.channel.send(`:white_check_mark: Your prompt has been added`);

  let ch = message.guild.promptsChannel;

  if(!ch) return console.log('Couldn\'t find prompts channel');

  const {RichEmbed} = require('discord.js');

  let embed = new RichEmbed()

  .setAuthor(message.author.tag, message.author.displayAvatarURL)
  .setDescription(text)
  .setFooter('ID: '+index)
  .setTimestamp(new Date());

  await ch.send({embed:embed}).catch(console.error);


};
