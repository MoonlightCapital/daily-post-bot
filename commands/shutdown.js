exports.run = async (client, message, args) => {
  if(message.author.id !== client.config.owner) return;

  await message.channel.send('Shutting down...');

  process.exit(0);
}
