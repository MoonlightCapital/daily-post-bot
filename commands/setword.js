exports.run = async (client, message, args) => {

  let word = args.join(' ');

  if(word.length > 1024)
    return message.reply(':x: The word is too long! It should be 1024 characters or less.');

  client.db.set('daily.word', word);

  message.channel.send(`:white_check_mark: Successfully set the word of the day to *${word}*`);
};
