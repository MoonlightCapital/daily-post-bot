exports.run = async (client, message, args) => {

  let element = client.db.get('daily.prompts').filter(e=>e.used == false)[0];

  if(!element)
    return message.channel.send(':warning: No element set for tomorrow');

  const {RichEmbed} = require('discord.js');

  let embed = new RichEmbed()
  .setTitle('Preview of tomorrow\'s quote')
  .setColor(0x7ED321)

  .setDescription(element.content)
  .addField('Word of the day', client.db.get('daily.word'))
  .setFooter(`Prompt n° ${element.id} will be used for tomorrow`)

  message.channel.send({embed:embed});
};
